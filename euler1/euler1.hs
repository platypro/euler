--Euler1 - "Multiples of 3 and 5"

{-
 I did this problem before deciding to document my procedure, so there is no brainstorming provided.

 This code works by implementing a function which generates a list of multiples up to a certain 
 amount (foldMult). It then adds the multiples of three, then the multiples of five, then subtracts
 the common multiples (as they have so far been added twice).
-}

module Euler1 where

--Add up all multiples
foldMult :: Integer -> Integer -> Integer
foldMult x top = foldl (+) 0 [0,x..top]

--Find solution
euler :: Integer -> Integer
euler top    = foldMult 3 top + foldMult 5 top - foldMult 15 top

main :: IO()
main = do
  print $ euler 999 --< Less than 1000, not 1000
