Project Euler Files
===================

These are my Haskell code files for project euler. This repo is not for solutions to problems, just a window onto someone else's thinking.

I will try committing every step of the way, even if the code doesn't work, to outline my thought process and how it changes as I work through the problems.

Each problem has its own sub-folder with the haskell code file, and possibly a brainstorming document. Each code file will implement a 'main' function which will take you directly to the problem solution, and a 'euler' function to get the answer for any value.

This project is just something fun for me to do so dont expect it to ever be updated ;)

![platypro's Euler badge](https://projecteuler.net/profile/platypro.png)
