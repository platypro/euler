module Euler7 where

-- Tests a prime given the previous primes
-- x: previous primes
-- y: prime to test
testprime :: [Integer] -> Integer -> Bool
testprime [] _ = True
testprime (x:xs) y = if (y `rem` x == 0) then False else testprime xs y 

-- Generates a list of primes
primelist :: [Integer] -> Integer -> Integer -> [Integer]
primelist x y top = 
  if y /= top then
    if testprime x y == True then 
      primelist (y:x) (y+1) top 
    else 
      primelist x (y+1) top
  else
    x