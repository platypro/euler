--Euler2 - Even Fibonacci numbers

{-
 I did this problem before deciding to document my procedure, so there is no brainstorming provided.

 This code works by implementing a function for finding a list of fibonacci numbers given the first
 number, the difference between first and second numbers, as well as a limit. It is a recursive 
 function, which cons the first number onto another 'fib' function which generates the rest of the
 sequence.

 Once the fibonacci sequence is generated, the even numbers are isolated, then folded to get the 
 answer.
-}

module Euler2 where

--Generates a fibonacci sequence
fib :: (Num a,Ord a) => a->a->a->[a]
fib x y z = if x < z then x : (fib (x+y) x z) else []

--Find solution
euler :: Integer -> Integer
euler top = foldl (+) 0 $ filter even $ fib 0 1 top

main :: IO()
main = do
  print $ euler 4000000 --< Four Million
